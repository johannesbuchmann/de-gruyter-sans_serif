class FontGlyph  < FontDbConnection
  belongs_to :glyph
  belongs_to :font
end
